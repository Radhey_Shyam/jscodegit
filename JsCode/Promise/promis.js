function f1(){
    console.log('f1')
}
function f2(){
    console.log('f2')
}

function main(){
    console.log("Program is start")
    setTimeout(f1,0)
    new Promise(function(resolve,reject){
        // resolve("I am a promish")
        reject("Reject promish")
    }).then((resolve)=>{
        console.log(resolve)
    }).catch((reject)=>{
        console.log(reject)
    })
    f2()
}

main()