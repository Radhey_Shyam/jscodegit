function asyncDemo(){
    console.log("function start")
    const response= fetch('https://api.github.com/users').then((res)=>{
        if(res.ok){
            return res.json()
        }
        else{
            return "Error in fetching data";
        }
    })
    
   return response
    
}

console.log("Start our program")
const data=asyncDemo();
console.log("After asyncDemo function call")
console.log(data)
console.log("End of program")
