async function asyncDemo(){
    console.log("asyncDemo function start")
    const response= await fetch('https://api.github.com/users')
    console.log("data are fetched")
    const retData= response.json().then((data)=>{
        console.log("Data converted to json")
        console.log(data) 
    })
    return response
}

console.log("Start our program")
const data=asyncDemo();
console.log("After asyncDemo function call")
console.log(data)
console.log("End of program")

