// ---Scope 
//  1. Block Scope
//  2. Function Scope
//  3. Global Scope

//------- Block Scope------------------
// {
//     var id=101
//     let name="Radhey"
//     const city="Ballia"
// }
// console.log(id)
// console.log(name)
// console.log(city)

//--------Function Scope---------------

// function f(){
//     var id=102
//     let name="Radhey"
//     const city="Ballia"
//     // console.log(id)
// }
// f()
// console.log(id)
// console.log(name)
// console.log(city)

//---------Global--------

// var id=102
// let name="Radhey"
// const city="Ballia"

// function f1(){
//     console.log(id)
//     console.log(name)
//     console.log(city)
// }
// f1()


//----- Reassignment---

// var id=102
// let name="Radhey"
// const city="Ballia"
// const arr=['a','b']

// id=205
// name="Shyam"
// // city+="Jhansi"
// arr.push('c')

// console.log(id)
// console.log(name)
// console.log(city)
// console.log(arr)

const fun={
    city : "Ballia",

    lol: console.log(this.city)
}
// fun.city="Jhansi"

console.log(fun.city)
console.log(fun.lol.bind(fun))


// Not Reassignment of const with object
// fun={
//     city:"Jhansi"
// }
// console.log(fun.city)
