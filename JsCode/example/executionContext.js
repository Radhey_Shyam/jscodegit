// console.log(n) // it show undefine because during memory execution phase var store undefine
// console.log(square)  // during memory execution phase function store wole function

// var n=2

// function square(side){
//     var ans=side*side
//     return ans
// }

// console.log(n)
// console.log(square)

// console.log(square(n))
// console.log(square(4))

//--------------------------------------------------------

console.log(n) // it show undefine because during memory execution phase var store undefine.
console.log(square)  //  during memory execution phase square is as a variable so it store undefine.

var n=2

var square=(side)=>{
    var ans=side*side
    return ans
}

console.log(n)
console.log(square)

console.log(square(n))
console.log(square(4))