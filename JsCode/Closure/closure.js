
// function x(){
//     let a=7
//     function y(){
//         console.log(a)
//     }
//     y()
//     a=100
//     return y
// }

// var z=x()
// console.log(z)
// z()

//------------------------------------------

// function f(){
//     var a=7
//     setTimeout(function(){
//         console.log(i)
//     },1000)
//     console.log("Hello")
// }
// f()

//--------------------------------------------

// function f(){
//     for(var i=0;i<=5;i++){
//         setTimeout(function(){
//             console.log(i)
//         },1000)
//     }
//     setTimeout(()=>{ console.log("radhey")},100)
//     console.log("Hello")
// }
// f()

//------------------------------------------------------
// when we use let in for loop and call to timeOut than for each closer it generate 
// a new copy of i variable at different different location because let is work in block 
// scope only.

// function f1(){
//     for(let i=0;i<=5;i++){
//         setTimeout(function(){
//             console.log(i)
//         },100)
//     }
//     setTimeout(()=>{ console.log("radhey")},100)
//     console.log("Hello")
// }
// f1()

//----------------------------------------------------------

// function f(){
//     for(var i=0;i<=5;i++){
//         function close(x){
//             setTimeout(function(){
//                 console.log(x)
//             },
//             x*1000)
//         }
//         close(i)
//     }
//     setTimeout(()=>{ console.log("radhey")},100)
//     console.log("Hello")
// }
// f()
